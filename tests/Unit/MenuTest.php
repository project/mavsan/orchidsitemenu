<?php
/**
 * MenuTest.php
 * Date: 21.04.2020
 * Time: 15:33
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Tests;

use Config;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use OrchidSiteMenu\Models\Menu;
use OrchidSiteMenu\Models\MenuItem;
use Tests\TestCase;

class MenuTest extends TestCase
{

    use RefreshDatabase;

    public function testGetLanguages()
    {
        Config::set('sitemenu.locales', []);
        Config::set('app.locale', 'sky');

        $this->assertIsArray(Menu::getLanguages());
        $this->assertCount(1, Menu::getLanguages());
        $this->assertSame('sky', Menu::getLanguages()[0]);

        Config::set('sitemenu.locales', 'app.locales');
        Config::set('app.locales', ['en', 'es']);

        $this->assertIsArray(Menu::getLanguages());
        $this->assertCount(2, Menu::getLanguages());
        $this->assertSame(['en', 'es'], Menu::getLanguages());
    }

    public function testGetTitle()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->make([
            'data' => [
                'en' => [
                    'title' => 'title',
                ],
                'ru' => [
                    'title' => 'пункт',
                ],
            ],
        ]);

        Config::set('app.locale', 'fr');
        $this->assertSame('title', $menu->getTitle('en'));
        $this->assertSame('пункт', $menu->getTitle('ru'));

        Config::set('app.locale', 'en');
        $this->assertSame('title', $menu->getTitle());
        Config::set('app.locale', 'ru');
        $this->assertSame('пункт', $menu->getTitle());

        $menu = Menu::factory()->make([]);
        Config::set('app.locale', 'bbb');
        $this->assertSame(__('sitemenu::item.empty.title'), $menu->getTitle());
    }

    public function testGetSlug()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->make([
            'data' => [
                'en' => [
                    'slug' => 'slug',
                ],
                'ru' => [
                    'slug' => 'ссылка',
                ],
            ],
        ]);

        Config::set('app.locale', 'fr');
        $this->assertSame('slug', $menu->getSlug('en'));
        $this->assertSame('ссылка', $menu->getSlug('ru'));

        Config::set('app.locale', 'en');
        $this->assertSame('slug', $menu->getSlug());
        Config::set('app.locale', 'ru');
        $this->assertSame('ссылка', $menu->getSlug());

        $menu = Menu::factory()->make([]);
        Config::set('app.locale', 'bbb');
        $this->assertSame(__('sitemenu::item.empty.slug'), $menu->getSlug());
    }

    public function testGetOpenInNewTab()
    {
        /** @var Menu $menu */
        $menu = Menu::factory()->make([
            'data' => [
                'en' => [
                    'new-tab' => 1,
                ],
                'ru' => [
                    'new-tab' => 0,
                ],
                'de' => [
                    'new-tab' => '1',
                ],
                'by' => [
                    'new-tab' => '0',
                ],
            ],
        ]);

        Config::set('app.locale', 'fr');
        $this->assertTrue($menu->getOpenInNewTab('en'));
        $this->assertFalse($menu->getOpenInNewTab('ru'));
        $this->assertTrue($menu->getOpenInNewTab('de'));
        $this->assertFalse($menu->getOpenInNewTab('by'));

        Config::set('app.locale', 'en');
        $this->assertTrue($menu->getOpenInNewTab());
        Config::set('app.locale', 'ru');
        $this->assertFalse($menu->getOpenInNewTab());
        Config::set('app.locale', 'de');
        $this->assertTrue($menu->getOpenInNewTab());
        Config::set('app.locale', 'by');
        $this->assertFalse($menu->getOpenInNewTab());
    }

    public function testGetMenuItemsAsOption()
    {
        Menu::factory()->create([
            'id'   => 1,
            'type' => 't',
            'sort' => 0,
            'data' => [
                'ru' => ['title' => 'l1 i1'],
            ],
        ]);

        Menu::factory()->create([
            'id'   => 2,
            'type' => 't',
            'sort' => 1,
            'data' => [
                'ru' => ['title' => 'l1 i2'],
            ],
        ]);

        Menu::factory()->create([
            'id'     => 3,
            'type'   => 't',
            'sort'   => 0,
            'parent' => 1,
            'data'   => [
                'ru' => ['title' => 'ti1 l2 i1'],
            ],
        ]);

        Menu::factory()->create([
            'id'     => 4,
            'type'   => 't',
            'sort'   => 1,
            'parent' => 1,
            'data'   => [
                'ru' => ['title' => 'ti1 l2 i2'],
            ],
        ]);

        Menu::factory()->create([
            'id'     => 5,
            'type'   => 't',
            'sort'   => 0,
            'parent' => 3,
            'data'   => [
                'ru' => ['title' => 'ti3 l3 i1'],
            ],
        ]);

        $data = resolve(Menu::class)->getMenuItemsAsOption('t');

        $this->assertSame([
            'i1' => 'l1 i1',
            'i3' => '-ti1 l2 i1',
            'i5' => '--ti3 l3 i1',
            'i4' => '-ti1 l2 i2',
            'i2' => 'l1 i2',
        ], $data);
    }

    public function testIsSystem()
    {
        /** @var Menu $item */
        $item = Menu::factory()->make(['id' => 1]);

        Config::set('sitemenu.systemIDs', '');
        $this->assertFalse($item->isSystem());

        Config::set('sitemenu.systemIDs', [1]);
        $this->assertTrue($item->isSystem());
    }

    public function testGetAll()
    {
        $data = [
            1 => [
                'id'     => 1,
                'type'   => 'mt',
                'parent' => 0,
                'sort'   => 0,
                'data'   => [
                    'ru' => [
                        'title'   => 'i1',
                        'slug'    => '/i1',
                        'classes' => '',
                        'styles'  => '',
                        'new-tab' => 1,
                    ],
                ],
            ],
            2 => [
                'id'     => 2,
                'type'   => 'mt',
                'parent' => 1,
                'sort'   => 0,
                'data'   => [
                    'ru' => [
                        'title'   => 'i1 s1',
                        'slug'    => '/i1s1',
                        'classes' => 'class-name',
                        'styles'  => 'some: style;',
                        'new-tab' => 0,
                    ],
                ],
            ],
            3 => [
                'id'     => 3,
                'type'   => 'mt',
                'parent' => 0,
                'sort'   => 1,
                'data'   => [
                    'ru' => [
                        'title'   => 'i2',
                        'slug'    => '/i2',
                        'classes' => '',
                        'styles'  => '',
                        'new-tab' => 0,
                    ],
                ],
            ],
        ];

        Config::set('sitemenu.systemIDs', [1]);

        Menu::factory()->create($data[1]);
        Menu::factory()->create($data[2]);
        Menu::factory()->create($data[3]);

        $all = resolve(Menu::class)->getAllLng('mt');

        $this->assertCount(3, $all);

        /** @var MenuItem $model */
        $model = $all[0];

        $this->assertSame([
            'id'       => 1,
            'parent'   => 0,
            'title'    => 'i1',
            'slug'     => '/i1',
            'classes'  => '',
            'styles'   => '',
            'new-tab'  => true,
            'sort'     => 0,
            'children' => [],
            'level'    => 0,
            'type'     => 'mt',
            'isSystem' => true,
        ], [
            'id'       => $model->getId(),
            'parent'   => $model->getParent(),
            'title'    => $model->getTitle(),
            'slug'     => $model->getSlug(),
            'classes'  => $model->getClasses(),
            'styles'   => $model->getStyles(),
            'new-tab'  => $model->isNewTab(),
            'sort'     => $model->getSort(),
            'children' => $model->getChildren()->toArray(),
            'level'    => $model->getLevel(),
            'type'     => $model->getType(),
            'isSystem' => $model->isSystem(),
        ]);

        $model = $all[1];

        $this->assertSame([
            'id'       => 2,
            'parent'   => 1,
            'title'    => 'i1 s1',
            'slug'     => '/i1s1',
            'classes'  => 'class-name',
            'styles'   => 'some: style;',
            'new-tab'  => false,
            'sort'     => 0,
            'children' => [],
            'level'    => 1,
            'type'     => 'mt',
            'isSystem' => false,
        ], [
            'id'       => $model->getId(),
            'parent'   => $model->getParent(),
            'title'    => $model->getTitle(),
            'slug'     => $model->getSlug(),
            'classes'  => $model->getClasses(),
            'styles'   => $model->getStyles(),
            'new-tab'  => $model->isNewTab(),
            'sort'     => $model->getSort(),
            'children' => $model->getChildren()->toArray(),
            'level'    => $model->getLevel(),
            'type'     => $model->getType(),
            'isSystem' => $model->isSystem(),
        ]);

        $model = $all[2];

        $this->assertSame([
            'id'       => 3,
            'parent'   => 0,
            'title'    => 'i2',
            'slug'     => '/i2',
            'classes'  => '',
            'styles'   => '',
            'new-tab'  => false,
            'sort'     => 1,
            'children' => [],
            'level'    => 0,
            'type'     => 'mt',
            'isSystem' => false,
        ], [
            'id'       => $model->getId(),
            'parent'   => $model->getParent(),
            'title'    => $model->getTitle(),
            'slug'     => $model->getSlug(),
            'classes'  => $model->getClasses(),
            'styles'   => $model->getStyles(),
            'new-tab'  => $model->isNewTab(),
            'sort'     => $model->getSort(),
            'children' => $model->getChildren()->toArray(),
            'level'    => $model->getLevel(),
            'type'     => $model->getType(),
            'isSystem' => $model->isSystem(),
        ]);
    }

    public function testIsIdInChildren()
    {
        /** @var Menu $menu1 */
        $menu1 = Menu::factory()->create(['id' => 1]);
        Menu::factory()->create(['id' => 2, 'parent' => 1]);
        Menu::factory()->create(['id' => 3, 'parent' => 1]);
        Menu::factory()->create(['id' => 4, 'parent' => 3]);
        Menu::factory()->create(['id' => 5, 'parent' => 4]);
        Menu::factory()->create(['id' => 6, 'parent' => 4]);
        Menu::factory()->create(['id' => 7]);

        /*
         1 =>
            2 =>
            3 =>
                4 =>
                    5 =>
                    6 =>
         7 =>
         */

        $this->assertFalse($menu1->isIdInChildren(7));
        $this->assertFalse($menu1->isIdInChildren(66));
        $this->assertTrue($menu1->isIdInChildren(6));
        $this->assertTrue($menu1->isIdInChildren(5));
        $this->assertTrue($menu1->isIdInChildren(4));
        $this->assertTrue($menu1->isIdInChildren(3));
        $this->assertTrue($menu1->isIdInChildren(2));
    }

    public function testGetTopMenuID()
    {
        /** @var Menu $menu1 */
        $menu1 = Menu::factory()->create(['id' => 1]);
        /** @var Menu $menu2 */
        $menu2 = Menu::factory()->create(['id' => 2, 'parent' => 1]);
        Menu::factory()->create(['id' => 3, 'parent' => 1]);
        /** @var Menu $menu4 */
        $menu4 = Menu::factory()->create(['id' => 4, 'parent' => 3]);
        Menu::factory()->create(['id' => 5, 'parent' => 4]);
        /** @var Menu $menu6 */
        $menu6 = Menu::factory()->create(['id' => 6, 'parent' => 4]);
        Menu::factory()->create(['id' => 7]);
        /** @var Menu $menu8 */
        $menu8 = Menu::factory()->create(['id' => 8, 'parent' => 7]);

        /*
         1 =>
            2 =>
            3 =>
                4 =>
                    5 =>
                    6 =>
         7 =>
            8 =>
         */

        $this->assertSame(1, $menu1->getTopMenuID());
        $this->assertSame(1, $menu2->getTopMenuID());
        $this->assertSame(1, $menu4->getTopMenuID());
        $this->assertSame(1, $menu6->getTopMenuID());
        $this->assertSame(7, $menu8->getTopMenuID());
    }

    public function testGetTreeChildren()
    {
        /** @var Menu $menu1 */
        $menu1 = Menu::factory()->create(['id' => 1]);
        Menu::factory()->create(['id' => 2, 'parent' => 1, 'sort' => 1]);
        Menu::factory()->create(['id' => 3, 'parent' => 1, 'sort' => 0]);
        Menu::factory()->create(['id' => 4, 'parent' => 3]);
        Menu::factory()->create(['id' => 5, 'parent' => 4, 'sort' => 0]);
        Menu::factory()->create(['id' => 6, 'parent' => 4, 'sort' => 1]);

        $tree = $menu1->getTreeChildren(0);

        Arr::get($tree, '0.id');

        /*
        $tree = [
            0 = [
                id = 3,
                ...
                children => [
                    0 => [
                        id = 4,
                        ...
                        children => [
                            0 = [
                                id = 5,
                                ...
                                children => []
                            ],
                            1 = [
                                id = 6,
                                ...
                                children => []
                            ],
                        ],
                    ],
                ],
            ],
            1 = [
                id = 2,
                ...
                children => [],
            ],
        ]
         */

        $this->assertCount(2, $tree);
        $this->assertSame(3, Arr::get($tree, '0.id'));
        $this->assertSame(2, Arr::get($tree, '1.id'));

        $this->assertIsArray(Arr::get($tree, '1.children')->toArray());
        $this->assertEmpty(Arr::get($tree, '1.children')->toArray());

        $this->assertIsArray(Arr::get($tree, '0.children')->toArray());
        $this->assertCount(1, Arr::get($tree, '0.children'));
        $this->assertSame(4, Arr::get($tree, '0.children.0.id'));
        $this->assertCount(2, Arr::get($tree, '0.children.0.children'));
        $this->assertSame(5, Arr::get($tree, '0.children.0.children.0.id'));
        $this->assertSame(6, Arr::get($tree, '0.children.0.children.1.id'));
    }

    public function testGetTreeAll()
    {
        Menu::factory()->create(['id' => 1, 'type' => 'tt', 'sort' => 0]);
        Menu::factory()->create(['id' => 2, 'type' => 'tt', 'parent' => 1, 'sort' => 1]);
        Menu::factory()->create(['id' => 3, 'type' => 'tt', 'parent' => 1, 'sort' => 0]);
        Menu::factory()->create(['id' => 4, 'type' => 'tt', 'parent' => 3]);
        Menu::factory()->create(['id' => 5, 'type' => 'tt', 'parent' => 4, 'sort' => 0]);
        Menu::factory()->create(['id' => 6, 'type' => 'tt', 'parent' => 4, 'sort' => 1]);
        Menu::factory()->create(['id' => 7, 'type' => 'tt', 'sort' => 1]);
        Menu::factory()->create(['id' => 8, 'type' => 'tb']);

        $tree = resolve(Menu::class)->getTreeAll('tt');

        $this->assertIsArray($tree->toArray());
        $this->assertCount(2, $tree);
        $this->assertSame(1, Arr::get($tree, '0.id'));
        $this->assertSame(7, Arr::get($tree, '1.id'));

        $this->assertIsArray(Arr::get($tree, '1.children')->toArray());
        $this->assertEmpty(Arr::get($tree, '1.children'));

        $this->assertIsArray(Arr::get($tree, '0.children')->toArray());
        $this->assertCount(2, Arr::get($tree, '0.children'));

        $this->assertSame(3, Arr::get($tree, '0.children.0.id'));
        $this->assertIsArray(Arr::get($tree, '0.children.0.children')->toArray());
        $this->assertCount(1, Arr::get($tree, '0.children.0.children'));

        $this->assertSame(4, Arr::get($tree, '0.children.0.children.0.id'));
        $this->assertIsArray(Arr::get($tree, '0.children.0.children.0.children')->toArray());
        $this->assertCount(2, Arr::get($tree, '0.children.0.children.0.children'));

        $this->assertSame(5, Arr::get($tree, '0.children.0.children.0.children.0.id'));
        $this->assertIsArray(Arr::get($tree, '0.children.0.children.0.children.0.children')->toArray());
        $this->assertEmpty(Arr::get($tree, '0.children.0.children.0.children.0.children'));

        $this->assertSame(6, Arr::get($tree, '0.children.0.children.0.children.1.id'));
        $this->assertIsArray(Arr::get($tree, '0.children.0.children.0.children.1.children')->toArray());
        $this->assertEmpty(Arr::get($tree, '0.children.0.children.0.children.1.children'));

        $this->assertSame(2, Arr::get($tree, '0.children.1.id'));
        $this->assertIsArray(Arr::get($tree, '0.children.1.children')->toArray());
        $this->assertEmpty(Arr::get($tree, '0.children.1.children'));
    }

    public function testResortOnDelete()
    {
        Menu::factory()->create(['id' => 1, 'type' => 'tt', 'sort' => 0]);
        Menu::factory()->create(['id' => 2, 'type' => 'tt', 'sort' => 1]);
        Menu::factory()->create(['id' => 3, 'type' => 'tt', 'sort' => 4]);
        Menu::factory()->create(['id' => 4, 'type' => 'tt', 'sort' => 2]);
        Menu::factory()->create(['id' => 5, 'type' => 'tt', 'sort' => 3]);
        Menu::factory()->create(['id' => 6, 'type' => 'tt', 'sort' => 5]);

        $this->assertCount(6, Menu::type('tt')->get());
        $this->assertEquals(4, Menu::find(3)->sort);
        $this->assertEquals(3, Menu::find(5)->sort);
        $this->assertEquals(5, Menu::find(6)->sort);

        Menu::find(4)->delete();

        $this->assertCount(5, Menu::type('tt')->get());
        $this->assertEquals(3, Menu::find(3)->sort);
        $this->assertEquals(2, Menu::find(5)->sort);
        $this->assertEquals(4, Menu::find(6)->sort);
    }

    public function testChangeSort()
    {
        Menu::factory()->create(['id' => 1, 'parent' => 0, 'type' => 'cs', 'sort' => 0]);
        Menu::factory()->create(['id' => 2, 'parent' => 0, 'type' => 'cs', 'sort' => 1]);
        Menu::factory()->create(['id' => 3, 'parent' => 0, 'type' => 'cs', 'sort' => 2]);
        Menu::factory()->create(['id' => 4, 'parent' => 0, 'type' => 'cs', 'sort' => 3]);

        $this->assertEquals(1, Menu::find(2)->sort);
        $this->assertEquals(3, Menu::find(4)->sort);

        Menu::find(2)->updateSort(0, 3, 1);

        $this->assertEquals(3, Menu::find(2)->sort);
        $this->assertEquals(1, Menu::find(4)->sort);
    }

    public function testChangeParent()
    {
        Menu::factory()->create(['id' => 1, 'parent' => 0, 'type' => 'tt', 'sort' => 0]);
        Menu::factory()->create(['id' => 2, 'parent' => 0, 'type' => 'tt', 'sort' => 1]);

        Menu::factory()->create(['id' => 3, 'parent' => 1, 'type' => 'tt', 'sort' => 0]);
        Menu::factory()->create(['id' => 4, 'parent' => 1, 'type' => 'tt', 'sort' => 1]);
        Menu::factory()->create(['id' => 5, 'parent' => 1, 'type' => 'tt', 'sort' => 2]);
        Menu::factory()->create(['id' => 6, 'parent' => 1, 'type' => 'tt', 'sort' => 3]);

        Menu::factory()->create(['id' => 7, 'parent' => 2, 'type' => 'tt', 'sort' => 0]);
        Menu::factory()->create(['id' => 8, 'parent' => 2, 'type' => 'tt', 'sort' => 1]);
        Menu::factory()->create(['id' => 9, 'parent' => 2, 'type' => 'tt', 'sort' => 2]);
        Menu::factory()->create(['id' => 10, 'parent' => 2, 'type' => 'tt', 'sort' => 3]);

        /** @var Menu $parent1 */
        $parent1 = Menu::find(1);
        /** @var Menu $parent2 */
        $parent2 = Menu::find(2);

        $this->assertCount(4, $parent1->children()->get());
        $this->assertCount(4, $parent2->children()->get());

        /** @var Menu $children */
        $children = Menu::find(4);
        // был родитель 1
        // был индекс 1
        $this->assertEquals(1, $children->parent);
        $this->assertEquals(1, $children->sort);

        $children->updateSort(2, 2, 1);

        // новый родитель 2
        // новый сортировочный индекс 2
        $children = Menu::find(4);
        $this->assertEquals(2, $children->parent);
        $this->assertEquals(2, $children->sort);

        // пункт меню переехал, кол-во подпунктов изменилось
        $this->assertCount(3, $parent1->children()->get());
        $this->assertCount(5, $parent2->children()->get());

        // должны измениться сортировочне индексы
        // родитель 1
        $this->assertEquals(0, Menu::find(3)->sort);
        $this->assertEquals(1, Menu::find(5)->sort);
        $this->assertEquals(2, Menu::find(6)->sort);
        // родитель 2
        $this->assertEquals(0, Menu::find(7)->sort);
        $this->assertEquals(1, Menu::find(8)->sort);
        $this->assertEquals(2, Menu::find(4)->sort);
        $this->assertEquals(3, Menu::find(9)->sort);
        $this->assertEquals(4, Menu::find(10)->sort);
    }
}
