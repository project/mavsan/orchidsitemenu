/**
 * Created by mavsan on 25.05.2020.
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

import Sortable from "sortablejs";
import axios from 'axios';

let activeElement = null, toContainer = null, newIndex, oldIndex;

function onSort(evt) {
  activeElement = evt.item;
  toContainer = evt.to;
  newIndex = evt.newIndex;
  oldIndex = evt.oldIndex;
}

function store() {
  if (activeElement === null || toContainer === null)
    return;

  axios.post(SM_STORE, {
    parent: toContainer.dataset.parentId,
    id: activeElement.dataset.id,
    newIndex: newIndex,
    oldIndex: oldIndex
  })

  activeElement = toContainer = null;
}

export default function () {
  let sortableOptions = {
    group: 'nested',
    animation: 150,
    fallbackOnBody: true,
    swapThreshold: 0.65,
    ghostClass: 'sm-items__empty',
    store: {
      get: function (sortable) {
      },
      set: function () {
        store();
      }
    },
    onSort: onSort
  };

  let smUl = document.querySelectorAll('.sm-items');

  for (let i = 0; i < smUl.length; i++) {
    new Sortable(smUl[i], sortableOptions);
  }
}
