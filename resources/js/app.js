/**
 * Created by mavsan on 25.05.2020.
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

import sortable from "./sortable";

document.addEventListener("turbo:load", function () {
  sortable();
})

// turbo:load не срабатывает, при загрузке страницы
sortable();
