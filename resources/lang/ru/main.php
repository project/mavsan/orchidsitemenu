<?php
/**
 * ru.php
 * языковой файл для основного экрана - выбор нужного типа меню
 * Date: 15.04.2020
 * Time: 19:48
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

return [
    'top' => 'Верхнее меню',
    'bottom' => 'Нижнее меню',
    'name' => 'Меню сайта'
];
