<?php
/** @var \OrchidSiteMenu\Models\MenuItem $treeItem */
?>
{{-- list of menu items screen --}}
@if(empty($tree))
    <div class="alert alert-primary" role="alert">
        {{ __('sitemenu::list.listEmpty') }}
    </div>
@else
    @if(!isset($level))
        <script>var SM_STORE = "{{ route('platform.site-menu.list', ['method' => 'storeSorted', 'menuType' => $menuType]) }}";</script>

        <div class="alert alert-info" role="alert">{{ __('sitemenu::list.immediateSaving') }}</div>

        <ul id="sm-items__root" class="sm-items__root sm-items mt-3" data-parent-id="0">
            @endif

            @foreach($tree as $treeItem)
                <li class="sm-items__one" data-id="{{ $treeItem->getId() }}">
                    @include('sitemenu::menu-list-item', ['treeItem' => $treeItem])

                    <ul class="sm-items" data-parent-id="{{ $treeItem->getId() }}">
                        @if($treeItem->getChildren()->count())
                            @include('sitemenu::menu-list', ['tree' => $treeItem->getChildren(), 'level' => isset($level) ? $level + 1 : 1])
                        @endif
                    </ul>
                </li>
            @endforeach

            @if(!isset($level))
        </ul>
    @endif
@endif
