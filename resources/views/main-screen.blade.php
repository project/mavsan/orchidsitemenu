{{-- select menu type screen --}}
<ul class="sm__main">
    @foreach($menus as $menuType => $menuTitle)
        <li><a href="{{ route('platform.site-menu.list', ['menuType' => $menuType]) }}">{{ __($menuTitle) }}</a></li>
    @endforeach
</ul>
