<?php
/** @var \OrchidSiteMenu\Models\MenuItem $treeItem */
?>
<div class="sm-items__data">
    <span class="sm-items__drg"><x-orchid-icon path="move" class="mr-2"/></span>

    <a href="{{ route('platform.site-menu.item', ['id' => $treeItem->getId(), 'menuType' => $menuType]) }}"
       class="sm-items__menu"
    >
        {{ $treeItem->getTitle() }}

        @if($treeItem->isSystem())
            <span class="text-muted text-lowercase mr-2">
                <strong>({{ __('sitemenu::item.system') }})</strong>
            </span>
        @endif
    </a>

    <span class="sm-items__rm">
        {!! \Orchid\Screen\Actions\Button::make('')->icon('trash')->method('removeMenu')->parameters(['id' => $treeItem->getId()]) !!}
    </span>
</div>
