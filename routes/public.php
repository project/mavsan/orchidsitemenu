<?php
/**
 * public.php
 * Date: 16.04.2020
 * Time: 18:52
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

declare(strict_types=1);

use OrchidSiteMenu\Http\Controllers\ResourceController;

$this->router->get('site-menu-resources/{patch}', [ResourceController::class, 'show'])
             ->where('patch', '.*')
             ->name('public');
