# Редактор меню сайта для Orchid Laravel

Сделать этот пакет навеяло создание (копирование из предыдущих проектов) в 100500 раз одного и того-же модуля админки - редактор меню сайта.

## Установка

Для Orchid версии 14:
В `composer.json` в секцию: `require` добавить: `"mavsan/orchid-site-menu": "^14"`, или выполнить команду:
`composer require mavsan/orchid-site-menu:^14`.

Для Orchid версии 10:
В `composer.json` в секцию: `require` добавить: `"mavsan/orchid-site-menu": "^10"`, или выполнить команду:
`composer require mavsan/orchid-site-menu:^10`.

Для Orchid версии 9:
В `composer.json` в секцию: `require` добавить: `"mavsan/orchid-site-menu": "^9"`, или выполнить команду: 
`composer require mavsan/orchid-site-menu:^9`. Инструкция изменена, переключитесь на последнюю метку тега 9, чтобы 
изучить актуальную инструкцию.

Для Orchid версии 7: выполнить команду `composer require mavsan/orchid-site-menu:^7` (инструкция немного отличается, переключайтесь на последний тег, там инструкция по этой версии).

Публикация конфигурации:

`php artisan vendor:publish --tag=site-menu-config`

Публикация языковых файлов:

`php artisan vendor:publish --tag=site-menu-translations`

Публикация шаблонов (views)

`php artisan vendor:publish --tag=site-menu-views`

Регистрация в меню Orchid (`app\Orchid\Composers\MainMenuComposer.php`):

```php
public function registerMainMenu(): array
{
    return [
        Menu::make(__('sitemenu::main.name'))
                ->icon('menu')
                ->route('platform.site-menu.main'),
    ];
}
```

Создать таблицу для хранения меню:

```php
php artisan migrate
```

В поставке есть только локализация `ru`, поэтому, чтобы не лицезреть в админстративной панели надписи вида 
`sitemenu::main.bottom` убедитесь, что в файле конфигурации `app.php` в параметре `locale` установлено значение `ru`.

## Маршруты и хлебные крошки

В файл маршрутов Orchid (`routes/platform.php`) необходимо добавить следующие маршруты (пути можно изменить):

```php
// Platform -> Site Menu
Route::screen('site-menu/{menuType}/item/add-to/{parent}', OrchidSiteMenu\Screens\MenuItemAddToParent::class)
    ->name('platform.site-menu.item.add-to-parent')
    ->breadcrumbs(function (Trail $trail, $menuType, $itemID) {
        return $trail
            ->parent('platform.site-menu.list', $menuType)
            ->push(__('Create'));
    });
// Platform -> Site Menu -> Site Menu Type
Route::screen('site-menu/{menuType}/item/{id}', OrchidSiteMenu\Screens\MenuItemScreen::class)
    ->name('platform.site-menu.item')
    ->breadcrumbs(function (Trail $trail, $menuType, $itemID) {
        return $trail
            ->parent('platform.site-menu.list', $menuType)
            ->push($itemID !== 'add' ? __('Edit') : __('Create'));
    });
// Platform -> Site Menu -> Site Menu Type -> Site Menu Item
Route::screen('site-menu/{menuType}/list', OrchidSiteMenu\Screens\MenuListScreen::class)
    ->name('platform.site-menu.list')
    ->breadcrumbs(function (Trail $trail, $menuType) {
        $label = __('sitemenu::main.'.$menuType) === 'sitemenu::main.'.$menuType
            ? config('sitemenu.menus.' . $menuType)
            : __('sitemenu::main.'.$menuType);

        return $trail
            ->parent('platform.site-menu.main')
            ->push($label, route('platform.site-menu.list', ['menuType' => $menuType]));
    });
// Platform -> Site Menu -> Site Menu Type -> Site Menu Item add to parent
Route::screen('site-menu', OrchidSiteMenu\Screens\MenuMainScreen::class)
    ->name('platform.site-menu.main')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('sitemenu::main.name'), route('platform.site-menu.main'));
    });
```

## Конфигурация

`locales` - где искать используемые на сайте локали. По-умолчанию `config/app.php`, ключ `locales`, должно быть 
массивом. Если ключа в конфигурации нет, то будет использован `app.locale`. Это означает, что если по-умолчанию сейчас 
`app.locale = en`, вы заполните меню, а потом решите сделать `app.locale = ru`, то надо заново пересоздать все меню, 
т.к. в базе сохранится именно как `en`.

`menus` - список нужных меню (верхнее, нижнее, слева, справа и т.д.), массив - ключ => выводимое в админке название (или 
ключ в языковом файле). По ключу этого массива потом будете запрашивать меню у модели.

`systemIDs` - массив, список ID пунктов меню, которые являются служебными. Например, структура меню такая: "О нас", 
"Доставка", "Пользовательское соглашение", "Каталог". Каталог - генерируется автоматически. Чтобы не вышло ситуации, что 
"Каталог" пропал - надо внести его ID сюда, и тогда его нельзя будет удалить в административной панели.

`show` - отображаемые элементы в редакторе пункта меню

## Использование

При построении меню для фронтэнда необходимо получить все пункты интересующего типа меню верхнего уровня, на выходе 
будет коллекция из экземпляров модели `\OrchidSiteMenu\Models\MenuItem`, содержащий дерево всего меню данного типа. 

```php
// По-умолчанию не отображаются пункты меню, отмеченные как скрытые для текущего языка
// получить все пункты меню для ТЕКУЩЕГО языка одним списком, т.е. строить дерево надо самому
$menuItems = resolve(OrchidSiteMenu\Models\Menu::class)->getTreeAll('needMenuType', $parent = 0, $level = 0, $showHidden = false);
// аналогично инструкции выше
$menuItems = OrchidSiteMenu\Models\Menu::getAllCurrLng('needMenuType', $parent = 0, $level = 0, $showHidden = false);
// получить все пункты меню для ТЕКУЩЕГО языка в виде дерево родитель-потомок-...-потомок
$tree = resolve(OrchidSiteMenu\Models\Menu::class)->getTreeAll('needMenuType', $parent = 0, $showHidden = false);
// аналогично инструкции выше
$tree = OrchidSiteMenu\Models\Menu::getTreeAllCurrLng('needMenuType', $parent = 0, $showHidden = false);
```
, где: `needMenuType` - одно из значений, указанных в файле конфигурации в `menus` (т.е. какое меню вам надо - верхнее, 
нижнее и т.д.)

При необходимости получить дерево для конкретного пункта меню, необходимо вызвать метод `getTreeChildren($level = 0, $showHidden = false)`, где 
`level` - номер следующего за текущим уровень меню (просто номер, если он нужен зачем-то), `$showHidden` - нужны ли скрытые пункты меню. 
Например, верхний уровень - 0, следующий - 1 и т.д. Именно так эти уровни формируются при вызове метода `getTreeAll('needMenuType')`. На выходе 
будет массив, содержащий информацию о потомках пункта меню, для которого был вызван метод `getTreeChildren`. При этом - 
информация о текущем пункте меню не будет включена в результат работы метода `getTreeChildren`.

Если есть необходимость кеширования меню, то надо создать слушателя событий (см. доку о Laravel) и подписаться на события
модели `OrchidSiteMenu\Models\Menu`. При формировании меню надо кешировать данные с каким-то ключем, например: `topmenu-ru` 
или `topmenu-en`. В слушателе событий, при возникновении событий `saved` и `deleted` - удалять кеш с этими ключами 
(если сайт мультиязычный, то удалять надо для всех языков сразу).
