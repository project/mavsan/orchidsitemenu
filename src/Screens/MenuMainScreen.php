<?php

namespace OrchidSiteMenu\Screens;

use Orchid\Screen\Action;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;

class MenuMainScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'MenuMain';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $this->name = __('sitemenu::main.name');

        return [
            'menus' => config('sitemenu.menus'),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::view('sitemenu::main-screen'),
        ];
    }
}
