<?php

namespace OrchidSiteMenu\Screens;

use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use OrchidSiteMenu\Models\Menu;
use Illuminate\Support\Facades\Route;

class MenuItemScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'MenuItemScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    protected $menuType;
    protected $addNew = false;
    protected $isSystem = false;

    /**
     * Query data.
     *
     * @param $menuType
     * @param $id
     *
     * @return array
     */
    public function query($menuType, $id): array
    {
        $this->menuType = $menuType;
        // add передается в том числе из экрана MenuItemAddToParent
        $this->addNew = $id === 'add';
        $this->name = $this->addNew ? __('sitemenu::item.name.add')
            : __('sitemenu::item.name.edit');

        if ($id === 'add') {
            $menu = new Menu();
        } else {
            /** @var Menu $menu */
            $menu =
                Menu::where('type', $menuType)->where('id', $id)->firstOrFail();
            $this->isSystem = $menu->isSystem();
        }

        $menuData = $menu->toArray();
        $menuData['parent'] = "i".$menu->parent;

        return [
            'menuType' => $menuType,
            'itemID'   => $id,
            'item'     => $menuData,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            DropDown::make(__('sitemenu::item.buttons.save'))
                    ->icon('menu')
                    ->list([
                        Button::make(__('sitemenu::item.buttons.saveAndBack'))
                              ->icon('arrow-left')
                              ->parameters([
                                  'screen-action' => 'backToList',
                              ])
                              ->method('save'),
                        Button::make(__('sitemenu::item.buttons.saveEndEdit'))
                              ->icon('note')
                              ->parameters([
                                  'screen-action' => 'edit',
                              ])
                              ->method('save'),
                        Button::make(__('sitemenu::item.buttons.saveAndAddNew'))
                              ->parameters([
                                  'screen-action' => 'addNew',
                              ])
                              ->icon('plus')
                              ->method('save'),
                        Button::make(__('sitemenu::item.buttons.saveAndAddChildren'))
                              ->parameters([
                                  'screen-action' => 'addChildren',
                              ])
                              ->icon('plus')
                              ->method('save'),
                    ]),
            Button::make(__('sitemenu::item.buttons.remove'))
                  ->icon('trash')
                  ->method('remove')
                  ->canSee(! $this->addNew && ! $this->isSystem),
        ];
    }

    /**
     * @return \Orchid\Screen\Layouts\Rows|\Orchid\Screen\Layouts\Tabs
     */
    protected function getLanguagesLayout()
    {
        $languages = Menu::getLanguages();

        if (count($languages) === 1) {
            return $this->genMenuLayout($languages[0]);
        } else {
            $data = [];

            foreach ($languages as $lang) {
                $data[$lang] = $this->genMenuLayout($lang, true);
            }

            return Layout::tabs($data);
        }
    }

    /**
     * @param string $lang
     *
     * @param bool   $multi
     *
     * @return \Orchid\Screen\Layouts\Rows
     */
    protected function genMenuLayout($lang, $multi = false)
    {
        $m = $multi ? " ($lang)" : '';

        return Layout::rows([
            Group::make([
                Input::make("item.data.$lang.title")
                     ->required()
                     ->title(__('sitemenu::item.title').$m),
                Input::make("item.data.$lang.slug")
                     ->title(__('sitemenu::item.slug').$m),
            ]),
            Group::make([
                Input::make("item.data.$lang.classes")
                     ->title(__('sitemenu::item.class'))
                     ->canSee(config('sitemenu.show.class')),
                Input::make("item.data.$lang.styles")
                     ->title(__('sitemenu::item.style'))
                     ->canSee(config('sitemenu.show.style')),
            ]),
            Group::make([
                CheckBox::make("item.data.$lang.new-tab")
                        ->placeholder(__('sitemenu::item.new-tab').$m)
                        ->sendTrueOrFalse(),
                CheckBox::make("item.data.$lang.hidden")
                    ->placeholder(__('sitemenu::item.hidden').$m)
                    ->sendTrueOrFalse(),
            ]),
        ]);
    }

    /**
     * Получение select options меню для выбора родителя
     *
     * @return array
     */
    protected function getMenuTree()
    {
        $options = resolve(Menu::class)->getMenuItemsAsOption($this->menuType);

        return ['i0' => __('sitemenu::item.topLevel')] + $options;
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Select::make('item.parent')
                      ->title('sitemenu::item.topLevel')
                      ->options($this->getMenuTree()),
            ]),
            $this->getLanguagesLayout(),
        ];
    }

    /**
     * Генерация правил валидации
     * @return array
     */
    protected function genRules(): array
    {
        $languages = Menu::getLanguages();

        $rules = [];
        foreach ($languages as $lang) {
            $rules["item.data.$lang.title"] = 'required';
        }

        return $rules;
    }

    /**
     * Генерация названий атрибутов для валидации
     * @return array
     */
    protected function genAttributes(): array
    {
        $languages = Menu::getLanguages();

        $labels = [];
        foreach ($languages as $lang) {
            $m = count($languages) > 1 ? " ($lang)" : '';
            $labels["item.data.$lang.title"] = __('sitemenu::item.title').$m;
            $labels["item.data.$lang.slug"] = __('sitemenu::item.slug').$m;
        }

        return $labels;
    }

    /**
     * Создание / обновление пункта меню
     *
     * @param                          $menuType
     * @param                          $menuID
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $menuType = $request->route('menuType');
        $menuID = $request->route('id');

        $this->validate(
            $request,
            $this->genRules(),
            [],
            $this->genAttributes()
        );

        $isNew = $menuID === 'add'
                 || Route::currentRouteName()
                    === 'platform.site-menu.item.add-to-parent';
        $menu = $isNew
            ? new Menu()
            : Menu::type($menuType)
                  ->where('id', $menuID)
                  ->firstOrFail();

        $data = $request->get('item');
        $data['parent'] = mb_substr($data['parent'], 1);

        if ($isNew) {
            // новый - делаем последним у родителя
            $data['sort'] =
                Menu::where('parent', $data['parent'])
                    ->get()
                    ->count();
        }

        if ($menu->isIdInChildren($data['parent'])) {
            Alert::error(__('sitemenu::item.newParentIsChildren'));

            return back();
        }

        $menu->fill($data);
        $menu->type = $menuType;
        $menu->save();

        Alert::success(__('sitemenu::item.'.($isNew ? 'created' : 'saved')));

        switch ($request->get('screen-action')) {
            case 'edit':
                return redirect()->route('platform.site-menu.item', [
                    'menuType' => $menuType,
                    'id'       => $menu->id,
                ]);
                break;

            case 'addNew':
                return redirect()->route(
                    'platform.site-menu.item.add-to-parent',
                    [
                        'menuType' => $menuType,
                        'parent'   => $menu->parent,
                    ]
                );
                break;

            case 'addChildren':
                return redirect()->route(
                    'platform.site-menu.item.add-to-parent',
                    [
                        'menuType' => $menuType,
                        'parent'   => $menu->id,
                    ]
                );
                break;

            case 'backToList':
            default:
                return redirect()->route('platform.site-menu.list', [
                    'menuType' => $menuType,
                ]);
        }
    }

    /**
     * Удаление пункта меню
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Request $request)
    {
        $menuType = $request->route('menuType');
        $menuID = $request->route('id');

        /** @var Menu $menu */
        $menu = Menu::type($menuType)
                    ->where('id', $menuID)
                    ->firstOrFail();

        if ($menu->isSystem()) {
            Alert::error(__('sitemenu::item.remove.system'));

            return back();
        }

        if ($menu->children()->count() > 0) {
            Alert::error(__('sitemenu::item.remove.children'));

            return back();
        }

        $menu->delete();

        Alert::success(__(
            'sitemenu::item.remove.success',
            ['title' => $menu->getTitle()]
        ));

        return redirect()->route(
            'platform.site-menu.list',
            ['menuType' => $menuType]
        );
    }
}
