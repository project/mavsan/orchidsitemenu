<?php
/**
 * MenuItemAddToParent.php
 * Date: 26.05.2020
 * Time: 9:29
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Screens;


class MenuItemAddToParent extends MenuItemScreen
{
    /**
     * Добавление пункта меню этому-же родителю
     * @param string $menuType
     * @param int $id
     * @return array
     */
    public function query($menuType, $id = 0): array
    {
        $query = parent::query($menuType, 'add');

        $query['item']['parent'] = "i$id";

        return $query;
    }

}
