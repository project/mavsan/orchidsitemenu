<?php

namespace OrchidSiteMenu\Screens;

use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use OrchidSiteMenu\Models\Menu;

class MenuListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'MenuListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    protected $menuType;

    /**
     * Query data.
     *
     * @param $menuType
     *
     * @return array
     */
    public function query($menuType): array
    {
        $this->menuType = $menuType;
        $this->name =
            __('sitemenu::main.'.$menuType) === 'sitemenu::main.'.$menuType
                ? config('sitemenu.menus.' . $menuType)
                : __('sitemenu::main.'.$menuType);
        $menu = new Menu();

        return [
            'menuType' => $menuType,
            'items'    => $menu->getAllLng($menuType, 0, 0, true),
            'tree'     => $menu->getTreeAll($menuType, 0, true),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('sitemenu::item.buttons.add'))
                ->icon('plus')
                ->route('platform.site-menu.item', [
                    'menuType' => $this->menuType,
                    'id'       => 'add',
                ]),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::view('sitemenu::menu-list'),
        ];
    }

    /**
     * Удаление пункта меню
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function removeMenu(Request $request)
    {
        $menuType = $request->route('menuType');
        $menuId = $request->get('id');
        /** @var Menu $menu */
        $menu = Menu::type($menuType)
                    ->where('id', $menuId)
                    ->firstOrFail();

        if ($menu->isSystem()) {
            Alert::error(__('sitemenu::item.remove.system'));

            return back();
        }

        if ($menu->children->count() > 0) {
            Alert::error(__('sitemenu::item.remove.children'));

            return back();
        }

        $menu->delete();

        Alert::success(__('sitemenu::item.remove.success',
            ['title' => $menu->getTitle()]));

        return redirect()->route('platform.site-menu.list',
            ['menuType' => $menuType]);
    }

    /**
     * Сохранение новой сортировки
     *
     * @param \Illuminate\Http\Request $request
     */
    public function storeSorted(Request $request)
    {
        /** @var Menu $menu */
        $menu = Menu::findOrFail($request->get('id'));

        $menu->updateSort($request->get('parent'),
            $request->get('newIndex'),
            $request->get('oldIndex'));
    }
}
