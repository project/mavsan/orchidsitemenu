<?php

namespace OrchidSiteMenu\Providers;

use File;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Orchid\Platform\Dashboard;
use OrchidSiteMenu\Helper\SiteMenu;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom($this->path('config/sitemenu.php'), 'sitemenu');
    }

    /**
     * Bootstrap services.
     *
     * @param \Orchid\Platform\Dashboard $dashboard
     *
     * @return void
     */
    public function boot(Dashboard $dashboard)
    {
        $this
            ->bootAll()
            ->registerProviders()
            ->bootAssets($dashboard);
    }

    protected function registerProviders()
    {
        foreach ($this->provides() as $provide) {
            $this->app->register($provide);
        }

        return $this;
    }

    public function provides()
    {
        return [
            RouteServiceProvider::class,
        ];
    }

    public function bootAssets(Dashboard $dashboard)
    {
        $css = SiteMenu::getPath('public/css/sitemenu.css');
        $js = SiteMenu::getPath('public/js/sitemenu.js');

        $manifest = $this->path('public/mix-manifest.json');
        if (File::exists($manifest)) {
            $manifest = json_decode(File::get($manifest), true);
        } else {
            $manifest = [];
        }

        if (File::exists($js)) {
            $file = Arr::get($manifest, '/js/sitemenu.js',
                '/js/sitemenu.js');
            $dashboard->registerResource('scripts',
                Dashboard::prefix('/site-menu-resources').$file);
        }

        if (File::exists($css)) {
            $file = Arr::get($manifest, '/css/sitemenu.css',
                '/css/sitemenu.css');
            $dashboard->registerResource('stylesheets',
                Dashboard::prefix('/site-menu-resources').$file);
        }

        return $this;
    }


    protected function bootAll() {
        $this->loadMigrationsFrom($this->path('database/migrations'));
        $this->loadTranslationsFrom($this->path('resources/lang'), 'sitemenu');
        $this->loadViewsFrom($this->path('resources/views'), 'sitemenu');

        $this->publishes([
            $this->path('config/sitemenu.php') => config_path('sitemenu.php'),
        ], 'site-menu-config');

        $this->publishes([
            $this->path('resources/lang') => resource_path('lang/vendor/sitemenu'),
        ], 'site-menu-translations');

        $this->publishes([
            $this->path('resources/views') => resource_path('views/vendor/sitemenu'),
        ], 'site-menu-views');

        return $this;
    }

    /**
     * Путь относительно библиотеки
     *
     * @param string $path
     *
     * @return string
     */
    protected function path(string $path = ''): string
    {
        return SiteMenu::getPath($path);
    }
}
