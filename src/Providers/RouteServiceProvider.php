<?php
/**
 * RoutesServiceProvider.php
 * Date: 16.04.2020
 * Time: 18:35
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Orchid\Platform\Dashboard;
use OrchidSiteMenu\Helper\SiteMenu;

class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        /*
         * Public
         */
        Route::domain((string) config('platform.domain'))
             ->prefix(Dashboard::prefix('/'))
             ->as('site-menu.')
             ->middleware(config('platform.middleware.private'))
             ->group(SiteMenu::getPath('routes/public.php'));
    }

}
