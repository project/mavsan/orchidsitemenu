<?php
/**
 * SiteMenu.php
 * Date: 16.04.2020
 * Time: 18:50
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Helper;


class SiteMenu
{
    /**
     * Путь к корню библиотеки
     *
     * @param string $path
     *
     * @return string
     */
    public static function getPath($path = ''): string
    {
        /* xdebug hack */
        $dir = __DIR__;
        $path = empty($path) ? '' : DIRECTORY_SEPARATOR.$path;

        return realpath($dir.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR
                        .'..'.$path);
    }
}
