<?php
/**
 * ManuItem.php
 * Date: 17.03.2021
 * Time: 10:19
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Models;

use ArrayAccess;
use JsonSerializable;

/**
 * Class MenuItem
 *
 * Интерфейс ArrayAccess реализован исключительно для совместимости при
 * переходе от получения данных в виде массива к полученю данных в виде
 * экземпляра класса
 *
 * @property-read int $id
 * @property-read int $parent
 * @property-read string $title
 * @property-read string $slug
 * @property-read string $classes
 * @property-read string $styles
 * @property-read bool $newTab
 * @property-read bool $hidden
 * @property-read int $sort
 * @property-read \Illuminate\Support\Collection|MenuItem[] $children
 * @property-read int $level
 * @property-read string $type
 * @property-read bool $isSystem
 *
 * @package OrchidSiteMenu\Models
 */
class MenuItem implements ArrayAccess, JsonSerializable
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $parent;
    /** @var string */
    protected $title;
    /** @var string */
    protected $slug;
    /** @var string */
    protected $classes;
    /** @var string */
    protected $styles;
    /** @var bool */
    protected $newTab;
    /** @var bool */
    protected $hidden;
    /** @var int */
    protected $sort;
    /** @var \Illuminate\Support\Collection|self[] */
    protected $children;
    /** @var int */
    protected $level;
    /** @var string */
    protected $type;
    /** @var bool */
    protected $isSystem;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getParent(): int
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getClasses(): string
    {
        return $this->classes;
    }

    /**
     * @param string $classes
     *
     * @return $this
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * @return string
     */
    public function getStyles(): string
    {
        return $this->styles;
    }

    /**
     * @param string $styles
     *
     * @return $this
     */
    public function setStyles($styles)
    {
        $this->styles = $styles;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNewTab(): bool
    {
        return $this->newTab;
    }

    /**
     * @param bool $newTab
     *
     * @return $this
     */
    public function setNewTab($newTab)
    {
        $this->newTab = $newTab;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     *
     * @return $this
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     *
     * @return $this
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return \Illuminate\Support\Collection|\OrchidSiteMenu\Models\MenuItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param \Illuminate\Support\Collection $children
     *
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSystem(): bool
    {
        return $this->isSystem;
    }

    /**
     * @param bool $isSystem
     *
     * @return $this
     */
    public function setIsSystem($isSystem)
    {
        $this->isSystem = $isSystem;

        return $this;
    }

    /**
     * Интерфейс ArrayAccess реализован исключительно для совместимости при
     * переходе от получения данных в виде массива к полученю данных в виде
     * экземпляра класса
     *
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        if(!$this->offsetExists($offset)) return null;

        switch ($offset) {
            case 'id':
                return $this->id;

            case 'parent':
                return $this->parent;

            case 'title':
                return $this->title;

            case 'slug':
                return $this->slug;

            case 'classes':
                return $this->classes;

            case 'styles':
                return $this->styles;

            case 'newTab':
                return $this->newTab;

            case 'hidden':
                return $this->hidden;

            case 'sort':
                return $this->sort;

            case 'children':
                return $this->children;

            case 'level':
                return $this->level;

            case 'type':
                return $this->type;

            case 'isSystem':
                return $this->isSystem;

            default:
                return null;
        }
    }

    /**
     * Интерфейс ArrayAccess реализован исключительно для совместимости при
     * переходе от получения данных в виде массива к полученю данных в виде
     * экземпляра класса
     *
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return in_array($offset, ['id', 'parent', 'title', 'slug', 'classes', 'styles', 'newTab', 'hidden', 'sort', 'children', 'level', 'type', 'isSystem']);
    }

    /**
     * Интерфейс ArrayAccess реализован исключительно для совместимости при
     * переходе от получения данных в виде массива к полученю данных в виде
     * экземпляра класса
     *
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        //
    }

    /**
     * Интерфейс ArrayAccess реализован исключительно для совместимости при
     * переходе от получения данных в виде массива к полученю данных в виде
     * экземпляра класса
     *
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        //
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'parent' => $this->parent,
            'title' => $this->title,
            'slug' => $this->slug,
            'classes' => $this->classes,
            'styles' => $this->styles,
            'newTab' => $this->newTab == 1,
            'hidden' => $this->hidden == 1,
            'sort' => $this->sort,
            'children' => $this->children,
            'level' => $this->level,
            'type' => $this->type,
            'isSystem' => $this->isSystem == 1,
        ];
    }
}
