<?php
/**
 * Menu.php
 * Date: 15.04.2020
 * Time: 18:15
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as ElCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use OrchidSiteMenu\Factory\MenuItemFactory;
use OrchidSiteMenu\Interfaces\Menu as MenuInterface;

//@formatter:off
/**
 * Class Menu
 *
 * @package OrchidSiteMenu\Models
 * @property int                                             $id
 * @property string                                          $type
 * @property int                                             $parent
 * @property array                                           $data
 * @property int                                             $sort
 * @property \Illuminate\Support\Carbon|null                 $created_at
 * @property \Illuminate\Support\Carbon|null                 $updated_at
 * @method static Builder sort()
 * @method static Builder type($menuType)
 * @method static Collection|MenuItem[] getAllCurrLng($menuType, $parent = 0, $level = 0, $showHidden = false)
 * @method static Collection|MenuItem[] getTreeAllCurrLng($menuType, $parent = 0, $showHidden = false)
 * @property-read ElCollection|\OrchidSiteMenu\Models\Menu[] $children
 * @property-read \OrchidSiteMenu\Models\Menu                $parentItem
 * @mixin Builder
 */
//@formatter:on
class Menu extends Model implements MenuInterface
{
    use HasFactory;

    protected $fillable = [
        'type',
        'parent',
        'data',
        'sort',
    ];

    protected $casts = [
        'parent' => 'int',
        'data'   => 'array',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleted(function (self $menu) {
            $menu->resort();
        });
    }

    /**
     * Пункты меню
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|self[]
     */
    public function children()
    {
        return $this->hasMany(self::class, 'parent', 'id');
    }

    /**
     * Родитель
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parentItem()
    {
        return $this->hasOne(self::class, 'id', 'parent');
    }

    /**
     * Скоуп для сортировки
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSort(Builder $query)
    {
        return $query->orderBy('sort');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $menuType
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeType(Builder $query, $menuType)
    {
        return $query->where('type', $menuType);
    }

    /**
     * @inheritDoc
     */
    public static function getLanguages(): array
    {
        config('sitemenu.locales');
        $languages = config(config('sitemenu.locales'));

        if (empty($languages)) {
            $languages = [config('app.locale')];
        }

        if (! is_array($languages)) {
            $languages = [$languages];
        }

        return $languages;
    }

    /**
     * @inheritDoc
     */
    public function getMenuItemsAsOption(
        $menuType,
        $parent = 0,
        $prep = '',
        $de = '-'
    ): array {
        $result = [];
        $menu = self::sort()
                    ->where('type', $menuType)
                    ->where('parent', $parent)
                    ->with('children')
                    ->get();

        /** @var Menu $menuItem */
        foreach ($menu as $menuItem) {
            $result['i'.$menuItem->id] = $prep.$menuItem->getTitle();

            $result = array_merge(
                $result,
                $this->getMenuItemsAsOption(
                    $menuType,
                    $menuItem->id,
                    $prep.$de,
                    $de
                )
            );
        }

        return $result;
    }

    /**
     * Получение данных о пункте меню для текущего языка
     *
     * @return \OrchidSiteMenu\Models\MenuItem
     */
    public function toArrayLng(): MenuItem
    {
        $menuItem = new MenuItem();
        $menuItem
            ->setId($this->id)
            ->setParent($this->parent)
            ->setTitle($this->getTitle())
            ->setSlug($this->getSlug())
            ->setClasses($this->getClasses())
            ->setStyles($this->getStyles())
            ->setNewTab($this->getOpenInNewTab())
            ->setHidden($this->getIsHidden())
            ->setSort($this->sort)
            ->setChildren(collect())
            ->setLevel(0)
            ->setType($this->type)
            ->setIsSystem($this->isSystem());

        return $menuItem;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $q
     * @param                                       $menuType
     * @param int                                   $parent
     * @param int                                   $level
     * @param bool                                  $showHidden
     *
     * @return array
     * @see getAllLng
     */
    public function scopeGetAllCurrLng(
        Builder $q,
        $menuType,
        $parent = 0,
        $level = 0,
        $showHidden = false
    ) {
        $model = new self();

        return $model->getAllLng($menuType, $parent, $level, $showHidden);
    }

    /**
     * @inheritDoc
     */
    public function getAllLng(
        $menuType,
        $parent = 0,
        $level = 0,
        $showHidden = false
    ): Collection {
        $result = collect();
        $menus = self::where('parent', $parent)
                     ->where('type', $menuType)
                     ->sort()
                     ->with('children')->get();

        /** @var Menu $menu */
        foreach ($menus as $menu) {
            $data = $menu->toArrayLng();

            if (! $showHidden && $data->isHidden()) {
                continue;
            }

            $data->setLevel($level);
            $result[] = $data;

            $result = $result
                ->merge($menu->getAllLng($menuType, $menu->id, $level + 1));
        }

        return $result;
    }

    /**
     * Возвращает или переданный $lang или текущий, установленный в app.locale
     *
     * @param string $lang
     *
     * @return string
     */
    protected function lang($lang): string
    {
        return empty($lang) ? config('app.locale') : $lang;
    }

    /**
     * @param string $lang
     * @param string $dataKey
     * @param string $default
     *
     * @return string|int
     */
    protected function getLngData($lang, $dataKey, $default = '')
    {
        return Arr::get($this->data, $this->lang($lang).".".$dataKey, $default);
    }

    /**
     * @inheritDoc
     */
    public function getTitle($lang = ''): string
    {
        return $this->getLngData(
            $lang,
            'title',
            __('sitemenu::item.empty.title')
        );
    }

    /**
     * @inheritDoc
     */
    public function getSlug($lang = ''): ?string
    {
        return $this->getLngData(
            $lang,
            'slug',
            __('sitemenu::item.empty.slug')
        );
    }

    /**
     * @inheritDoc
     */
    public function getClasses($lang = ''): ?string
    {
        return $this->getLngData(
            $lang,
            'classes',
            ''
        );
    }

    /**
     * @inheritDoc
     */
    public function getStyles($lang = ''): ?string
    {
        return $this->getLngData(
            $lang,
            'styles',
            ''
        );
    }

    /**
     * @inheritDoc
     */
    public function getOpenInNewTab($lang = ''): bool
    {
        $nt = $this->getLngData($lang, 'new-tab');

        return $nt === 1 || $nt === '1' || $nt === true || $nt === 'true';
    }

    /**
     * @inheritDoc
     */
    public function getIsHidden($lang = ''): bool
    {
        $hidden = $this->getLngData($lang, 'hidden');

        return $hidden === 1 || $hidden === '1' || $hidden === true
               || $hidden === 'true';
    }

    /**
     * Является ли текущий пункт меню "системным"
     *
     * @return bool
     */
    public function isSystem(): bool
    {
        $systemIDs = config('sitemenu.systemIDs');
        $systemIDs = ! is_array($systemIDs) ? [$systemIDs] : $systemIDs;

        return in_array($this->id, $systemIDs);
    }

    /**
     * @inheritDoc
     */
    public function isIdInChildren(int $menuItemId): bool
    {
        /** @var self $item */
        foreach ($this->children as $item) {
            if ($item->id == $menuItemId) {
                return true;
            } else {
                $result = $item->isIdInChildren($menuItemId);

                if ($result) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getTreeChildren($level = 0, $showHidden = false): Collection
    {
        $res = collect();
        $children = $this->children()->sort()->with('children')->get();

        /** @var self $item */
        foreach ($children as $item) {
            $data = $item->toArrayLng();

            if (! $showHidden && $data->isHidden()) {
                continue;
            }

            $data->setLevel($level);
            $data->setChildren($item->getTreeChildren($level + 1, $showHidden));
            $res[] = $data;
        }

        return $res;
    }

    /**
     * Получнние ИД верхнего уровня меню
     * @return int
     */
    public function getTopMenuID(): int
    {
        return
            $this->parent === 0
                ? $this->id
                : $this->parentItem->getTopMenuID();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $q
     * @param                                       $menuType
     * @param int                                   $parent
     * @param bool                                  $showHidden
     *
     * @return Collection
     * @see getTreeAll
     */
    public function scopeGetTreeAllCurrLng(
        Builder $q,
        $menuType,
        $parent = 0,
        $showHidden = false
    ) {
        return resolve(self::class)->getTreeAll($menuType, $parent,
            $showHidden);
    }

    /**
     * Построение всего меню как массива
     *
     * @param string $menuType
     * @param int    $parent
     * @param bool   $showHidden
     *
     * @return Collection|\OrchidSiteMenu\Models\MenuItem[]
     */
    public function getTreeAll(
        $menuType,
        $parent = 0,
        $showHidden = false
    ): Collection {
        $menu = Menu::sort()
                    ->type($menuType)
                    ->where('parent', $parent)
                    ->with('children')
                    ->get();
        $res = collect();

        /** @var Menu $item */
        foreach ($menu as $item) {
            $data = $item->toArrayLng();

            if (! $showHidden && $data->isHidden()) {
                continue;
            }

            $data->setChildren($item->getTreeChildren(1, $showHidden));
            $res[] = $data;
        }

        return $res;
    }

    /**
     * Пункт меню изменил сортировку или родителя
     *
     * @param int $parentID
     * @param int $newIndex
     * @param int $oldIndex
     */
    public function updateSort($parentID, $newIndex, $oldIndex)
    {
        if ($parentID == $this->parent) {
            // пункт меню переехал в пределах родителя
            $this->resortCurrParent($newIndex, $oldIndex);
        } else {
            // пункт меню переехал к другому родителю
            $oldParent = $this->parent;
            $this->resortMoveToNewParent($parentID, $newIndex);
            $this->resortOldParent($oldParent);
        }
    }

    /**
     * Пункт меню переехал в пределах родителя
     *
     * @param int $newIndex
     * @param int $oldIndex
     */
    protected function resortCurrParent($newIndex, $oldIndex)
    {
        /** @var self $menuTo */
        $menuTo = self::where('parent', $this->parent)
                      ->where('type', $this->type)
                      ->where('sort', $newIndex)->first();
        if($menuTo) {
            $menuTo->sort = $oldIndex;
            $menuTo->save();
        }

        $this->sort = $newIndex;
        $this->save();

        $ni = 0;
        Menu::where('parent', $this->parent)
            ->where('type', $this->type)
            ->sort()
            ->get()
            ->each(function (Menu $menu) use (&$ni) {
               $menu->sort = $ni++;
               $menu->save();
            });
    }

    /**
     * Пункт переехал к новому родителю, сохранение и сортировка у нового
     * родителя
     *
     * @param $parentId
     * @param $newIndex
     */
    protected function resortMoveToNewParent($parentId, $newIndex)
    {
        self::where('parent', $parentId)
            ->where('type', $this->type)
            ->sort()
            ->each(function (Menu $menu) use ($newIndex) {
                if ($menu->sort >= $newIndex) {
                    $menu->sort++;
                    $menu->save();
                }
            });

        $this->parent = $parentId;
        $this->sort = $newIndex;
        $this->save();
    }

    /**
     * Пункт меню переехал к новому родителю, пересортировка пунктов меню у
     * старого родителя
     *
     * @param int $oldParentId
     */
    protected function resortOldParent($oldParentId)
    {
        $i = 0;

        self::where('parent', $oldParentId)
            ->where('type', $this->type)
            ->sort()
            ->get()
            ->each(function (Menu $menu) use (&$i) {
                $menu->sort = $i++;
                $menu->save();
            });
    }

    /**
     * Полная пересортировка в пределах родителя
     *
     * @return void
     */
    public function resort()
    {
        self::where('parent', $this->parent)
            ->sort()
            ->get()
            ->each(function (self $menu, $key) {
                $menu->sort = $key;
                $menu->save();
            });
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return MenuItemFactory::new();
    }
}
