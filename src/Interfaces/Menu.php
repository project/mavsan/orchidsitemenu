<?php
/**
 * Menu.php
 * Date: 21.04.2020
 * Time: 15:41
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Interfaces;

use Illuminate\Support\Collection;

interface Menu
{
    /**
     * Используемые сайтом языки
     * @return array
     */
    public static function getLanguages(): array;

    /**
     * Выборка всех пунктов меню как select options, в качестве первого символа
     * ключа должен быть символ i, чтобы пхп не сортировал по этому ключу
     * автоматически
     *
     * @param string $menuType тип меню (конфиг sitemenu.menus)
     * @param int    $parent   ид родителя
     * @param string $prep     что добавляется перед названием пункта меню (для
     *                         индикации вложенности)
     * @param string $de       символ, который будет добавлять в $prep в каждой
     *                         итерации
     *
     * @return array
     */
    public function getMenuItemsAsOption(
        $menuType,
        $parent = 0,
        $prep = '',
        $de = '-'
    ): array;

    /**
     * Получение всех пунктов меню списком
     *
     * @param string $menuType
     * @param int    $parent
     * @param int    $level
     * @param bool   $showHidden
     *
     * @return Collection|\OrchidSiteMenu\Models\MenuItem[]
     */
    public function getAllLng($menuType, $parent = 0, $level = 0, $showHidden = false): Collection;

    /**
     * Получение подписи пункта меню. Если задан $lang, то будет возвращен
     * именно для этого языка, иначе текущий язык, значение конфига app.locale
     *
     * @param string $lang
     *
     * @return string
     */
    public function getTitle($lang = ''): string;

    /**
     * Получение ссылки пункта меню. Если задан $lang, то будет возвращен
     * именно для этого языка, иначе текущий язык, значение конфига app.locale
     *
     * @param string $lang
     *
     * @return string
     */
    public function getSlug($lang = ''): ?string;

    /**
     * Получение стилей пункта меню. Если задан $lang, то будет возвращен
     * именно для этого языка, иначе текущий язык, значение конфига app.locale
     *
     * @param string $lang
     *
     * @return string
     */
    public function getStyles($lang = ''): ?string;

    /**
     * Получение классов пункта меню. Если задан $lang, то будет возвращен
     * именно для этого языка, иначе текущий язык, значение конфига app.locale
     *
     * @param string $lang
     *
     * @return string
     */
    public function getClasses($lang = ''): ?string;

    /**
     * Получение признака - открывать ли в новом окне пункта меню. Если задан
     * $lang, то будет возвращен именно для этого языка, иначе текущий язык,
     * значение конфига app.locale
     *
     * @param string $lang
     *
     * @return bool
     */
    public function getOpenInNewTab($lang = ''): bool;

    /**
     * Получение признака - скрыт или нет пукнт меню
     * @param string $lang
     *
     * @return bool
     */
    public function getIsHidden($lang = ''): bool;

    /**
     * Является ли переданный ID одним из потомков
     *
     * @param int $menuItemId
     *
     * @return bool
     */
    public function isIdInChildren(int $menuItemId): bool;

    /**
     * Построение потомков текущего пункта меню
     *
     * @param int  $level
     * @param bool $showHidden
     *
     * @return Collection|\OrchidSiteMenu\Models\MenuItem[]
     */
    public function getTreeChildren($level = 0, $showHidden = false): Collection;

    /**
     * Получение ИД верхнего уровня меню
     * @return int
     */
    public function getTopMenuID(): int;

    /**
     * Построение всего меню как массива
     *
     * @param string $menuType
     * @param int    $parent
     * @param bool   $showHidden
     *
     * @return Collection|\OrchidSiteMenu\Models\MenuItem[]
     */
    public function getTreeAll($menuType, $parent = 0, $showHidden = false): Collection;
}
