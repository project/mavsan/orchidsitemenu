<?php

namespace OrchidSiteMenu\Factory;

use Illuminate\Database\Eloquent\Factories\Factory;
use OrchidSiteMenu\Models\Menu;

class MenuItemFactory extends Factory
{
    protected $model = Menu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent' => 0,
            'type' => 'menuType',
            'data' => [
                'ru' => [
                    'title' => $this->faker->words(3, true),
                    'slug' => '',
                    'new-tab' => 0,
                ],
                'en' => [
                    'title' => $this->faker->words(4, true),
                    'slug' => '',
                    'new-tab' => 0,
                ],
            ],
            'sort' => $this->faker->randomDigit
        ];
    }
}
