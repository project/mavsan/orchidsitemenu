<?php
/**
 * ResourceController.php
 * Date: 16.04.2020
 * Time: 18:53
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace OrchidSiteMenu\Http\Controllers;


use Orchid\Attachment\MimeTypes;
use OrchidSiteMenu\Helper\SiteMenu;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class ResourceController
{
    /**
     * @var SplFileInfo|null
     */
    private $resource;

    public function show(string $path)
    {
        $dir = SiteMenu::getPath('public');

        $resources = (new Finder())
            ->ignoreUnreadableDirs()
            ->followLinks()
            ->in($dir)
            ->files()
            ->path(dirname($path))
            ->name(basename($path));

        $iterator = tap($resources->getIterator())
            ->rewind();

        $this->resource = $iterator->current();

        abort_if(is_null($this->resource), 404);

        $mime = new MimeTypes();
        $mime = $mime->getMimeType($this->resource->getExtension());

        return response()->file($this->resource->getRealPath(), [
            'Content-Type'  => $mime ?? 'text/plain',
            'Cache-Control' => 'public, max-age=31536000',
        ]);
    }
}
